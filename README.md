# Scales

Scales is a preset for the Moodle activity database.

## Demo

https://fdagner.de/moodle/mod/data/view.php?d=2&perpage=1000

## Getting started

Download the source code and zip the files WITHOUT parent folder. Create a "Database" activity in Moodle and then upload the ZIP file in the "Presets" tab under "Import".

Fields can be added to customize the template. The fields must be mandatory.

The questions/statements are to be added in the template for list view and single view:

<code>let array = ["Die Fortbildung hat mich inspiriert", "Die Präsentation war anschaulich", "Die Informationen helfen mir"];
let colors = ["#ff0080", "#08d2fb", "#29d23a", "#9534eb", "#d45d02", "#0264d4", "#838587", "#00b36e", "#e82102",  "#02e8dd"]</code>

## Language Support

The preset is available in German. 

## Description

With scales, the feedback can be compared with the average and the min/max range

## License

https://creativecommons.org/licenses/by/4.0/

## Screenshots

<img width="400" alt="single view" src="/screenshots/listenansicht.png">
